package com.naga.share.market.fundamental.analysis.adaptor.company;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class CompanyDAO {
	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	/**
	 * Method to find a Company by its name
	 * 
	 * @param companyName
	 * @return Company
	 */
	public Company findByCompanyName(String companyName) {
		logger.info("CompanyDAO - findByCompanyName Method - start");
		TypedQuery<Company> query = entityManager.createNamedQuery(AdaptorConstants.COMPANY_FIND_BY_COMPANY_NAME,
				Company.class);
		query.setParameter(AdaptorConstants.COMPANY_NAME, companyName);
		Company company = null;
		try {
			company = query.getSingleResult();
			logger.debug("CompanyDAO - findByCompanyName Method - {}", company.getCompanyName());
		} catch (NoResultException noResultException) {
			logger.error("CompanyDAO - Company details not found.");
		}
		logger.info("CompanyDAO - findByCompanyName Method - end");
		return company;
	}

	/**
	 * Method to delete a Company by its name
	 * 
	 * @param companyName
	 */
	public void deleteByCompanyName(String companyName) {
		logger.info("CompanyDAO - deleteByCompanyName Method - start");
		entityManager.remove(findByCompanyName(companyName));
		logger.info("CompanyDAO - deleteByCompanyName Method - start");
	}

	/**
	 * Method to insert a Company
	 * 
	 * @param company
	 */
	public void insertCompany(Company company) {
		logger.info("CompanyDAO - insertCompany Method - start");
		entityManager.persist(company);
		logger.info("CompanyDAO - insertCompany Method - end");
	}

	/**
	 * Method to update a Company
	 * 
	 * @param company
	 */
	public void updateCompany(Company company) throws NoDataException {
		logger.info("CompanyDAO - updateCompany Method - start");
		try {
			entityManager.merge(company);
		} catch (IllegalArgumentException illegalArgumentException) {
			logger.error("CompanyDAO - Company details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}
		logger.info("CompanyDAO - updateCompany Method - end");
	}

	/**
	 * Method to find all Companies
	 * 
	 * @return List<Company>
	 */
	public List<Company> findAllCompanies() {
		logger.info("CompanyDAO - findAll Method - Start");
		TypedQuery<Company> query = entityManager.createNamedQuery(AdaptorConstants.COMPANY_FIND_ALL_COMPANIES,
				Company.class);
		logger.info("CompanyDAO - findAll Method - end");
		return query.getResultList();
	}
}
