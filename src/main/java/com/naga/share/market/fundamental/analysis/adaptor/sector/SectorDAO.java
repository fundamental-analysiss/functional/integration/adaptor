package com.naga.share.market.fundamental.analysis.adaptor.sector;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.sector.entity.Sector;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class SectorDAO {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	/**
	 * Method to find a sector by its name
	 * 
	 * @param sectorName
	 * @return Sector
	 */
	public Sector findBySectorName(String sectorName) {
		logger.info("SectorDAO - findBySectorName Method - start");
		TypedQuery<Sector> query = entityManager.createNamedQuery(AdaptorConstants.SECTOR_FIND_BY_SECTOR_NAME,
				Sector.class);
		query.setParameter(AdaptorConstants.SECTOR_NAME, sectorName);
		Sector sector = null;
		try {
			sector = query.getSingleResult();
			logger.debug("SectorDAO - findBySectorName Method - {}", sector.getSectorName());
		} catch (NoResultException noResultException) {
			logger.error("SectorDAO - Sector details not found.");
		}
		logger.info("SectorDAO - findBySectorName Method - end");
		return sector;
	}

	/**
	 * Method to delete a sector by its name
	 * 
	 * @param sectorName
	 */
	public void deleteBySectorName(String sectorName) {
		logger.info("SectorDAO - deleteBySectorName Method - start");
		entityManager.remove(findBySectorName(sectorName));
		logger.info("SectorDAO - deleteBySectorName Method - start");
	}

	/**
	 * Method to insert a sector
	 * 
	 * @param sector
	 */
	public void insertSector(Sector sector) {
		logger.info("SectorDAO - insertSector Method - start");
		entityManager.persist(sector);
		logger.info("SectorDAO - insertSector Method - end");
	}

	/**
	 * Method to update a Sector
	 * 
	 * @param sector
	 * @return
	 */
	public void updateSector(Sector sector) throws NoDataException {
		logger.info("SectorDAO - updateSector Method - start");
		try {
			entityManager.merge(sector);
		} catch (IllegalArgumentException illegalArgumentException) {
			logger.error("SectorDAO - Sector details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}
		logger.info("SectorDAO - updateSector Method - end");
	}

	/**
	 * Method to find all Sectors
	 * 
	 * @return List<Sector>
	 */
	public List<Sector> findAllSectors() {
		logger.info("SectorDAO - findAll Method - Start");
		TypedQuery<Sector> query = entityManager.createNamedQuery(AdaptorConstants.SECTOR_FIND_ALL_SECTORS,
				Sector.class);
		logger.info("SectorDAO - findAll Method - end");
		return query.getResultList();
	}

}
