package com.naga.share.market.fundamental.analysis.adaptor.industry;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.industry.entity.Industry;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class IndustryDAO {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	/**
	 * Method to find a Industry by its name
	 * 
	 * @param industryName
	 * @return Industry
	 */
	public Industry findByIndustryName(String industryName) {
		logger.info("IndustryDAO - findByIndustryName Method - start");
		TypedQuery<Industry> query = entityManager.createNamedQuery(AdaptorConstants.INDUSTRY_FIND_BY_INDUSTRY_NAME,
				Industry.class);
		query.setParameter(AdaptorConstants.INDUSTRY_NAME, industryName);
		Industry industry = null;
		try {
			industry = query.getSingleResult();
			logger.debug("IndustryDAO - findByIndustryName Method - {}", industry.getIndustryName());
		} catch (NoResultException noResultException) {
			logger.error("IndustryDAO - Industry details not found.");
		}
		logger.info("IndustryDAO - findByIndustryName Method - end");
		return industry;
	}

	/**
	 * Method to delete a Industry by its name
	 * 
	 * @param industryName
	 */
	public void deleteByIndustryName(String industryName) {
		logger.info("IndustryDAO - deleteByIndustryName Method - start");
		entityManager.remove(findByIndustryName(industryName));
		logger.info("IndustryDAO - deleteByIndustryName Method - start");
	}

	/**
	 * Method to insert a industry
	 * 
	 * @param industry
	 */
	public void insertIndustry(Industry industry) {
		logger.info("IndustryDAO - insertIndustry Method - start");
		entityManager.persist(industry);
		logger.info("IndustryDAO - insertIndustry Method - end");
	}

	/**
	 * Method to update a industry
	 * 
	 * @param industry
	 */
	public void updateIndustry(Industry industry) throws NoDataException {
		logger.info("IndustryDAO - updateIndustry Method - start");
		try {
			entityManager.merge(industry);
		} catch (IllegalArgumentException illegalArgumentException) {
			logger.error("IndustryDAO - Industry details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}
		logger.info("IndustryDAO - updateIndustry Method - end");
	}

	/**
	 * Method to find all Industries
	 * 
	 * @return List<Industry>
	 */
	public List<Industry> findAllIndustries() {
		logger.info("IndustryDAO - findAll Method - Start");
		TypedQuery<Industry> query = entityManager.createNamedQuery(AdaptorConstants.INDUSTRY_FIND_ALL_INDUSTRIES,
				Industry.class);
		logger.info("IndustryDAO - findAll Method - end");
		return query.getResultList();
	}

}
