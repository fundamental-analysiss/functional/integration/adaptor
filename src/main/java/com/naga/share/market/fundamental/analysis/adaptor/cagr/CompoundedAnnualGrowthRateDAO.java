package com.naga.share.market.fundamental.analysis.adaptor.cagr;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.company.CompanyDAO;
import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.cagr.entity.CompoundedAnnualGrowthRate;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class CompoundedAnnualGrowthRateDAO {
	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	@Autowired
	CompanyDAO companyDAO;

	/**
	 * 
	 * Method to get CompoundedAnnualGrowthRate analyzed details
	 * 
	 * @param company
	 * @param cagrYear
	 * @return CompoundedAnnualGrowthRate
	 */
	public CompoundedAnnualGrowthRate findByCompanyNameAndCAGRYears(Company company, int cagrStartYear,
			int cagrEndYear) {
		logger.info("CompoundedAnnualGrowthRateDAO - findByCompanyNameAndCAGRYear Method - start");
		TypedQuery<CompoundedAnnualGrowthRate> query = entityManager.createNamedQuery(
				AdaptorConstants.CAGR_FIND_BY_COMPANY_AND_CAGR_YEARS, CompoundedAnnualGrowthRate.class);
		query.setParameter(AdaptorConstants.COMPANY, company);
		query.setParameter(AdaptorConstants.ANALYSIS_START_YEAR, cagrStartYear);
		query.setParameter(AdaptorConstants.ANALYSIS_END_YEAR, cagrEndYear);
		CompoundedAnnualGrowthRate compoundedAnnualGrowthRate = null;
		try {
			compoundedAnnualGrowthRate = query.getSingleResult();
			logger.debug("CompoundedAnnualGrowthRateDAO - findByCompanyNameAndCAGRYear Method - {}",
					compoundedAnnualGrowthRate.getCompany().getCompanyName());
		} catch (NoResultException noResultException) {
			logger.error(
					"CompoundedAnnualGrowthRateDAO - findByCompanyNameAndCAGRYear Method - CompoundedAnnualGrowthRate analyzed details not found.");
		}
		logger.info("CompoundedAnnualGrowthRateDAO - findByCompanyNameAndCAGRYear Method - end");
		return compoundedAnnualGrowthRate;
	}

	/**
	 * Method to delete a CompoundedAnnualGrowthRate analyzed details by company
	 * details and CompoundedAnnualGrowthRate year
	 * 
	 * @param company
	 * @param cagrYear
	 */
	public void deleteByCompanyNameAndCAGRYear(Company company, int cagrStartYear, int cagrEndYear) {
		logger.info("CompoundedAnnualGrowthRateDAO - deleteByCompanyNameAndCAGRYear Method - start");
		entityManager.remove(findByCompanyNameAndCAGRYears(company, cagrStartYear, cagrEndYear));
		logger.info("CompoundedAnnualGrowthRateDAO - deleteByCompanyNameAndCAGRYear Method - End");
	}

	/**
	 * Method to insert a CompoundedAnnualGrowthRate Analyzed Details
	 * 
	 * @param compoundedAnnualGrowthRate
	 */
	public void insertCAGRAnalyzedDetails(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) {
		logger.info("CompoundedAnnualGrowthRateDAO - insertCAGRAnalyzedDetails Method - start");
		entityManager.persist(compoundedAnnualGrowthRate);
		logger.info("CompoundedAnnualGrowthRateDAO - insertCAGRAnalyzedDetails Method - end");
	}

	/**
	 * Method to update a CompoundedAnnualGrowthRate Analyzed Details
	 * 
	 * @param compoundedAnnualGrowthRate
	 */
	public void updateCAGRDetails(CompoundedAnnualGrowthRate compoundedAnnualGrowthRate) throws NoDataException {
		logger.info("CompoundedAnnualGrowthRateDAO - updateCAGRDetails Method - start");
		try {
			entityManager.merge(compoundedAnnualGrowthRate);
		} catch (IllegalArgumentException illegalArgumentException) {
			logger.error("CompoundedAnnualGrowthRateDAO - updateCAGRDetails analyzed details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}
		logger.info("CompoundedAnnualGrowthRateDAO - updateCAGRDetails Method - end");
	}

	/**
	 * Method to find all CompoundedAnnualGrowthRate analyzed details
	 * 
	 * @return List<CompoundedAnnualGrowthRate>
	 */
	public List<CompoundedAnnualGrowthRate> findAllCAGRDetails() {
		logger.info("CompoundedAnnualGrowthRateDAO - findAllCAGRDetails Method - Start");
		TypedQuery<CompoundedAnnualGrowthRate> query = entityManager
				.createNamedQuery(AdaptorConstants.CAGR_FIND_ALL_CAGR_ANALYZED, CompoundedAnnualGrowthRate.class);
		logger.info("CompoundedAnnualGrowthRateDAO - findAllCAGRDetails Method - end");
		return query.getResultList();
	}

	/**
	 * Method to find all CAGR analyzed years for a company
	 * 
	 * @return List<Annual>
	 * @throws NoDataException
	 */
	public List<CompoundedAnnualGrowthRate> findAllCAGRAnalyzedYearsForACompany(Company company)
			throws NoDataException {
		logger.info("CompoundedAnnualGrowthRateDAO - findAllCAGRAnalyzedYearsForACompany Method - Start");
		TypedQuery<CompoundedAnnualGrowthRate> query = entityManager.createNamedQuery(
				AdaptorConstants.CAGR_FIND_ALL_CAGR_ANALYZED_DETAILS_FOR_A_COMPANY, CompoundedAnnualGrowthRate.class);
		query.setParameter(AdaptorConstants.COMPANY, companyDAO.findByCompanyName(company.getCompanyName()));
		logger.info("CompoundedAnnualGrowthRateDAO - findAllCAGRAnalyzedYearsForACompany Method - end");
		return query.getResultList();
	}
}
