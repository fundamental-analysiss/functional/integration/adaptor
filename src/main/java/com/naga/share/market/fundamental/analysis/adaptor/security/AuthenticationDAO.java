package com.naga.share.market.fundamental.analysis.adaptor.security;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.security.entity.Authentication;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class AuthenticationDAO {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	/**
	 * Method to find a Authentication by the analyzerId
	 * 
	 * @param analyzerId
	 * @return Analyzer
	 */
	public Authentication findByAnalyzerId(String analyzerId) {
		logger.info("AuthenticationDAO - findByAnalyzerId Method - start");
		TypedQuery<Authentication> query = entityManager
				.createNamedQuery(AdaptorConstants.AUTHENTICATION_FIND_BY_ANALYZER_ID, Authentication.class);
		query.setParameter(AdaptorConstants.ANALYZER_ID, analyzerId);
		Authentication authentication = null;
		try {
			authentication = query.getSingleResult();
			logger.debug("AuthenticationDAO - findByAnalyzerId Method -{}", authentication.getAnalyzerId());
		} catch (NoResultException exception) {
			logger.debug("AuthenticationDAO - analyzer details not found.");
		}
		logger.info("AuthenticationDAO - findByAnalyzerId Method - end");
		return authentication;
	}

	/**
	 * Method to insert a Authentication
	 * 
	 * @param analyzerName
	 * @return
	 */
	public void insertAuthentication(Authentication authentication) {
		logger.info("AuthenticationDAO - insertAuthentication Method - start");
		entityManager.persist(authentication);
		logger.info("AuthenticationDAO - insertAuthentication Method - end");
	}

	/**
	 * Method to delete a Authentication by their analyzerId
	 * 
	 * @param analyzerId
	 * @throws NoDataException
	 */
	public void deleteByAnalyzerId(String analyzerId) {
		logger.info("AuthenticationDAO - deleteByAnalyzerId Method - start");
		entityManager.remove(findByAnalyzerId(analyzerId));
		logger.info("AuthenticationDAO - deleteByAnalyzerId Method - end");
	}

	/**
	 * Method to delete all
	 */
	public void deleteAll() {
		logger.info("AuthenticationDAO - deleteAll Method - start");
		Query query = entityManager.createQuery("DELETE FROM Authentication A");
		int num = query.executeUpdate();
		logger.info("AuthenticationDAO - deleteAll Method Rows Deleted : {}", num);
		logger.info("AuthenticationDAO - deleteAll Method - end");
	}

	public void deleteExpiredData() {
		logger.info("AuthenticationDAO - deleteExpiredData Method - start");
		Query query = entityManager
				.createQuery("DELETE FROM Authentication A WHERE A.oneTimePassExpiration < CURRENT_TIME");
		int num = query.executeUpdate();
		logger.info("AuthenticationDAO - deleteExpiredData Method Rows Deleted : {}", num);
		logger.info("AuthenticationDAO - deleteExpiredData Method - end");
	}
}
