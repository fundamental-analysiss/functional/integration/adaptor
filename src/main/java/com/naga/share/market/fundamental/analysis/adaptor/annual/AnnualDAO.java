package com.naga.share.market.fundamental.analysis.adaptor.annual;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.company.CompanyDAO;
import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.annual.entity.Annual;
import com.naga.share.market.fundamental.analysis.model.company.entity.Company;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class AnnualDAO {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	@Autowired
	CompanyDAO companyDAO;

	/**
	 * 
	 * Method to get Annually analyzed details
	 * 
	 * @param company
	 * @param analysisYear
	 * @return Annual
	 */
	public Annual findByCompanyAndAnalysisYear(Company company, int analysisYear) {
		logger.info("AnnualDAO - findByCompanyNameAndReportYear Method - start");
		TypedQuery<Annual> query = entityManager
				.createNamedQuery(AdaptorConstants.ANNUAL_FIND_BY_COMPANY_AND_ANALYSIS_YEAR, Annual.class);
		query.setParameter(AdaptorConstants.COMPANY, companyDAO.findByCompanyName(company.getCompanyName()));
		query.setParameter(AdaptorConstants.ANALYSIS_YEAR, analysisYear);
		Annual annual = null;
		try {
			annual = query.getSingleResult();
			logger.debug("AnnualDAO - findByCompanyNameAndReportYear Method - {}",
					annual.getCompany().getCompanyName());
		} catch (NoResultException noResultException) {
			logger.error("AnnualDAO - findByCompanyNameAndReportYear Method - Annually analyzed details not found.");
		}
		logger.info("AnnualDAO - findByCompanyNameAndReportYear Method - end");
		return annual;
	}

	/**
	 * Method to delete a Annually analyzed details by company details and analysis
	 * year
	 * 
	 * @param company
	 * @param analysisYear
	 * @throws NoDataException
	 */
	public void deleteByCompanyAndAnalysisYear(Company company, int analysisYear) throws NoDataException {
		logger.info("AnnualDAO - deleteByCompanyAndAnalysisYear Method - start");
		Annual annual = findByCompanyAndAnalysisYear(company, analysisYear);
		if (ValidationUtils.isNotNull(annual)) {
			entityManager.remove(annual);
		} else {
			logger.error("AnnualDAO - deleteByCompanyAndAnalysisYear Annually analyzed details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}

		logger.info("AnnualDAO - deleteByCompanyAndAnalysisYear Method - End");
	}

	/**
	 * Method to insert a Annual Analyzed Details
	 * 
	 * @param annual
	 */
	public void insertAnnuallyAnalyzedDetails(Annual annual) {
		logger.info("AnnualDAO - insertAnnuallyAnalyzedDetails Method - start");
		entityManager.persist(annual);
		logger.info("AnnualDAO - insertAnnuallyAnalyzedDetails Method - end");
	}

	/**
	 * Method to update a Annual Analyzed Details
	 * 
	 * @param annual
	 */
	public void updateAnnuallyAnalyzedDetails(Annual annual) throws NoDataException {
		logger.info("AnnualDAO - updateAnnuallyAnalyzedDetails Method - start");
		try {
			entityManager.merge(annual);
		} catch (IllegalArgumentException illegalArgumentException) {
			logger.error("AnnualDAO - Annually analyzed details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}
		logger.info("AnnualDAO - updateAnnuallyAnalyzedDetails Method - end");
	}

	/**
	 * Method to find all Annually analyzed details
	 * 
	 * @return List<Annual>
	 */
	public List<Annual> findAllAnnuallyAnalyzedDetails() {
		logger.info("AnnualDAO - findAllAnnuallyAnalyzedDetails Method - Start");
		TypedQuery<Annual> query = entityManager.createNamedQuery(AdaptorConstants.ANNUAL_FIND_ALL_ANNUALLY_ANALYZED,
				Annual.class);
		logger.info("AnnualDAO - findAllAnnuallyAnalyzedDetails Method - end");
		return query.getResultList();
	}

	/**
	 * Method to find all Annually analyzed details for a company
	 * 
	 * @return List<Annual>
	 * @throws NoDataException
	 */
	public List<Annual> findAllAnnuallyAnalyzedDetailsForACompany(Company company) throws NoDataException {
		logger.info("AnnualDAO - findAllAnnuallyAnalyzedDetailsForACompany Method - Start");
		TypedQuery<Annual> query = entityManager.createNamedQuery(
				AdaptorConstants.ANNUAL_FIND_ALL_ANNUALLY_ANALYZED_DETAILS_FOR_A_COMPANY, Annual.class);
		query.setParameter(AdaptorConstants.COMPANY, companyDAO.findByCompanyName(company.getCompanyName()));
		logger.info("AnnualDAO - findAllAnnuallyAnalyzedDetailsForACompany Method - end");
		return query.getResultList();
	}

	/**
	 * Method to find all Annually analyzed years for a company
	 * 
	 * @return List<Annual>
	 * @throws NoDataException
	 */
	public List<Annual> findAllAnnuallyAnalyzedYearsForACompany(Company company) throws NoDataException {
		logger.info("AnnualDAO - findAllAnnuallyAnalyzedYearsForACompany Method - Start");
		TypedQuery<Annual> query = entityManager.createNamedQuery(
				AdaptorConstants.ANNUAL_FIND_ALL_ANNUALLY_ANALYZED_DETAILS_FOR_A_COMPANY, Annual.class);
		query.setParameter(AdaptorConstants.COMPANY, companyDAO.findByCompanyName(company.getCompanyName()));
		logger.info("AnnualDAO - findAllAnnuallyAnalyzedYearsForACompany Method - end");
		return query.getResultList();
	}

	/**
	 * Method to find all Annually analyzed details for a company between two years
	 * 
	 * @return List<Annual>
	 * @throws NoDataException
	 */
	public List<Annual> findAnnuallyAnalyzedDetailsForACompanyBtwYears(Company company, int analysisStartYear,
			int analysisEndYear) throws NoDataException {
		logger.info("AnnualDAO - findAnnuallyAnalyzedDetailsForACompanyBtwYears Method - Start");
		TypedQuery<Annual> query = entityManager.createNamedQuery(
				AdaptorConstants.ANNUAL_FIND_ANNUALLY_ANALYZED_DETAILS_FOR_A_COMPANY_BTW_YEARS, Annual.class);
		query.setParameter(AdaptorConstants.COMPANY, companyDAO.findByCompanyName(company.getCompanyName()));
		query.setParameter(AdaptorConstants.ANALYSIS_START_YEAR, analysisStartYear);
		query.setParameter(AdaptorConstants.ANALYSIS_END_YEAR, analysisEndYear);
		logger.info("AnnualDAO - findAnnuallyAnalyzedDetailsForACompanyBtwYears Method - end");
		return query.getResultList();
	}

}
