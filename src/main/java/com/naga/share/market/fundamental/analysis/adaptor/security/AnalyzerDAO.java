package com.naga.share.market.fundamental.analysis.adaptor.security;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.exception.constants.ExceptionsConstants;
import com.naga.share.market.fundamental.analysis.model.security.entity.Analyzer;

/**
 * @author Nagaaswin S
 *
 */
@Repository
@Transactional
public class AnalyzerDAO {
	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	EntityManager entityManager;

	/**
	 * Method to find a Analyzer by their Id
	 * 
	 * @param analyzerId
	 * @return Analyzer
	 */
	public Analyzer findByAnalyzerId(String analyzerId) {
		logger.info("AnalyzerDAO - findByAnalyzerId Method - start");
		TypedQuery<Analyzer> query = entityManager.createNamedQuery(AdaptorConstants.ANALYZER_FIND_BY_ANALYZER_ID,
				Analyzer.class);
		query.setParameter(AdaptorConstants.ANALYZER_ID, analyzerId);
		Analyzer analyzer = null;
		try {
			analyzer = query.getSingleResult();
			logger.debug("AnalyzerDAO - findByAnalyzerId Method - {}", analyzer.getAnalyzerId());
		} catch (NoResultException exception) {
			logger.debug("AnalyzerDAO - analyzer details not found.");
		}
		logger.info("AnalyzerDAO - findByAnalyzerId Method - end");
		return analyzer;
	}

	/**
	 * Method to insert a Analyzer
	 * 
	 * @param analyzerName
	 * @return
	 */
	public void insertAnalyzer(Analyzer analyzer) {
		logger.info("AnalyzerDAO - insertAnalyzer Method - start");
		entityManager.persist(analyzer);
		logger.info("AnalyzerDAO - insertAnalyzer Method - end");
	}

	/**
	 * Method to update a Analyzer
	 * 
	 * @param analyzerName
	 * @return
	 */
	public void updateAnalyzer(Analyzer analyzer) throws NoDataException {
		logger.info("AnalyzerDAO - updateAnalyzer Method - start");
		try {
			entityManager.merge(analyzer);
		} catch (IllegalArgumentException e) {
			logger.error("AnalyzerDAO - analyzer details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}
		logger.info("AnalyzerDAO - updateAnalyzer Method - end");
	}

	/**
	 * Method to delete a Analyzer by their Id
	 * 
	 * @param analyzerId
	 * @throws NoDataException
	 */
	public void deleteByAnalyzerId(String analyzerId) {
		logger.info("AnalyzerDAO - deleteByAnalyzerId Method - start");
		entityManager.remove(findByAnalyzerId(analyzerId));
		logger.info("AnalyzerDAO - deleteByAnalyzerId Method - end");
	}

	/**
	 * Method to find a Analyzer by their emailId and Date Of Birth
	 * 
	 * @param emailId
	 * @param DOB
	 * @return Analyzer
	 * @throws NoDataException
	 */
	public Analyzer findByEmailIdAndDOB(String emailId, LocalDate DOB) throws NoDataException {
		logger.info("AnalyzerDAO - findByEmailIdAndDOB Method - start");
		TypedQuery<Analyzer> query = entityManager.createNamedQuery(AdaptorConstants.ANALYZER_FIND_BY_EMAIL_ID_AND_DOB,
				Analyzer.class);
		query.setParameter(AdaptorConstants.EMAIL_ID, emailId);
		query.setParameter(AdaptorConstants.DOB, DOB);
		Analyzer analyzer = null;
		try {
			analyzer = query.getSingleResult();
			logger.debug("AnalyzerDAO - findByEmailIdAndDOB Method - {}", analyzer.getAnalyzerId());
		} catch (NoResultException exception) {
			logger.error("AnalyzerDAO - analyzer details not found.");
			throw new NoDataException(ExceptionsConstants.NO_DATA_ERROR);
		}

		logger.info("AnalyzerDAO - findByEmailIdAndDOB Method - end");
		return analyzer;
	}

	/**
	 * Method to find all Analyzers
	 * 
	 * @return List<Analyzer>
	 */
	public List<Analyzer> findAll() {
		logger.info("AnalyzerDAO - findAll Method - Start");
		TypedQuery<Analyzer> query = entityManager.createNamedQuery(AdaptorConstants.ANALYZER_FIND_ALL_ANALYZERS,
				Analyzer.class);
		List<Analyzer> allAnalyzers = query.getResultList();
		logger.debug("AnalyzerDAO - findAll Method - All analyzers - {}", allAnalyzers);
		logger.info("AnalyzerDAO - findAll Method - end");
		return allAnalyzers;
	}

	/**
	 * Method to find a Analyzer by their emailId
	 * 
	 * @param emailId
	 * @return Analyzer
	 * 
	 */
	public Analyzer findByEmailId(String emailId) {
		logger.info("AnalyzerDAO - findByEmailId Method - start");
		TypedQuery<Analyzer> query = entityManager.createNamedQuery(AdaptorConstants.ANALYZER_FIND_BY_EMAIL_ID,
				Analyzer.class);
		query.setParameter(AdaptorConstants.EMAIL_ID, emailId);
		Analyzer analyzer = null;
		try {
			analyzer = query.getSingleResult();
			logger.debug("AnalyzerDAO - findByEmailId Method - {}", analyzer.getAnalyzerId());
		} catch (NoResultException exception) {
			logger.error("AnalyzerDAO - analyzer details not found.");
		}
		logger.info("AnalyzerDAO - findByEmailId Method - end");
		return analyzer;
	}
}
