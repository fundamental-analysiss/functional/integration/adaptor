package com.naga.share.market.fundamental.analysis.adaptor.constants;

public class AdaptorConstants {

	private AdaptorConstants() {
	}

//	#QUERIES
	public static final String ANALYZER_FIND_BY_ANALYZER_ID = "analyzerFindByAnalyzerId";
	public static final String ANALYZER_FIND_BY_EMAIL_ID_AND_DOB = "analyzerFindByEmailIdAndDOB";
	public static final String ANALYZER_FIND_BY_EMAIL_ID = "analyzerFindByEmailId";
	public static final String ANALYZER_FIND_ALL_ANALYZERS = "analyzerFindAllAnalyzers";
	public static final String AUTHENTICATION_FIND_BY_ANALYZER_ID = "AuthFindByAnalyzerId";
	public static final String SECTOR_FIND_ALL_SECTORS = "sectorFindAllSectors";
	public static final String SECTOR_FIND_BY_SECTOR_NAME = "sectorFindBySectorName";
	public static final String INDUSTRY_FIND_ALL_INDUSTRIES = "industryFindAllIndustries";
	public static final String INDUSTRY_FIND_BY_INDUSTRY_NAME = "industryFindByIndustryName";
	public static final String COMPANY_FIND_ALL_COMPANIES = "companyFindAllCompanies";
	public static final String COMPANY_FIND_BY_COMPANY_NAME = "companyFindByCompanyName";
	public static final String ANNUAL_FIND_ALL_ANNUALLY_ANALYZED = "annualFindAllAnnuallyAnalyzed";
	public static final String ANNUAL_FIND_ALL_ANNUALLY_ANALYZED_DETAILS_FOR_A_COMPANY = "annualFindAllAnnuallyAnalyzedDetailsForACompany";
	public static final String ANNUAL_FIND_ALL_ANNUALLY_ANALYZED_YEARS_FOR_A_COMPANY = "annualFindAllAnnuallyAnalyzedYearsForACompany";
	public static final String ANNUAL_FIND_ANNUALLY_ANALYZED_DETAILS_FOR_A_COMPANY_BTW_YEARS = "annualFindAnnuallyAnalyzedDetailsForACompanyBtwYears";
	public static final String ANNUAL_FIND_BY_COMPANY_AND_ANALYSIS_YEAR = "annualFindByCompanyAndAnalysisYear";
	public static final String CAGR_FIND_BY_COMPANY_AND_CAGR_YEARS = "cagrFindByCompanyAndCAGRYears";
	public static final String CAGR_FIND_ALL_CAGR_ANALYZED = "cagrFindAllCAGRAnalyzed";
	public static final String CAGR_FIND_ALL_CAGR_ANALYZED_DETAILS_FOR_A_COMPANY = "cagrFindAllCAGRAnalyzedDetailsForACompany";

//	#common
	public static final String EMAIL_ID = "emailId";
	public static final String DOB = "DOB";
	public static final String ANALYZER_ID = "analyzerId";
	public static final String SECTOR_NAME = "sectorName";
	public static final String INDUSTRY_NAME = "industryName";
	public static final String COMPANY_NAME = "companyName";
	public static final String COMPANY = "company";
	public static final String ANALYSIS_YEAR = "analysisYear";
	public static final String ANALYSIS_START_YEAR = "analysisStartYear";
	public static final String ANALYSIS_END_YEAR = "analysisEndYear";

//	#Error
	public static final String SECTOR_NOT_FOUND = "sector not found for given sector name.";

}
